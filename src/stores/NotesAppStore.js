import dispatcher from "../dispatcher/Dispatcher";
import { EventEmitter } from "events";
import Immutable from "immutable";
import * as NotesAppActions from "../actions/NotesAppActions";

class NotesAppStore extends EventEmitter {
    constructor() {
        super();
        this.data = Immutable.Map();
        this.data = this.data.set('notes', Immutable.List());
        this.data = this.data.set('showEditDialog', false);
        this.data = this.data.set('saveStatus', null);
        this.data = this.data.set('noteData', Immutable.fromJS({}));
    }

    removeNotes(ids) {
        ids.forEach(id => {
            const indexToRemove = this.getNotes().findIndex(listItem => {
                return listItem.get('id') === id;
            });
            this.data = this.data.deleteIn(['notes', indexToRemove]);
        });
    }

    handleActions(action) {
        switch (action.type) {
            case NotesAppActions.NOTES_APP_ACTIONS.LOAD_NOTES: {
                this.data = this.data.set('notes', action.data);
                this.emit("storeUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.SHOW_EDIT_DIALOG: {
                this.data = this.data.set('showEditDialog', true);
                if (action.data) {
                    this.data = this.data.setIn(['noteData', 'id'], action.data.get('id'));
                    this.data = this.data.setIn(['noteData', 'type'], action.data.get('type'));
                    this.data = this.data.setIn(['noteData', 'title'], action.data.get('title'));
                    this.data = this.data.setIn(['noteData', 'content'], action.data.get('content'));
                } else {
                    // initiate note type
                    this.data = this.data.setIn(['noteData', 'type'], 'note');
                }
                this.emit("storeDialogUpdated");
                this.emit("storeNoteDataUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.CLOSE_EDIT_DIALOG: {
                this.data = this.data.set('showEditDialog', false);
                this.data = this.data.set('saveStatus', null);
                this.data = this.data.set('noteData', Immutable.fromJS({}));
                this.emit("storeDialogUpdated");
                this.emit("storeNoteDataUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.DELETE_NOTE:
            case NotesAppActions.NOTES_APP_ACTIONS.SEND_BACK_NOTE: {
                const itemToUpdate = this.getNotes().findIndex(listItem => {
                    return listItem.get('id') === action.data.get('id');
                });
                this.data = this.data.setIn(['notes', itemToUpdate], action.data);
                this.emit("storeUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.REMOVE_NOTE: {
                const ids = [action.data.get('id')];
                this.removeNotes(ids);
                this.emit("storeUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.SET_NOTE_VALUE: {
                this.data = this.data.setIn(['noteData', action.key], action.value);
                this.emit("storeNoteDataUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.SET_NOTE_COLOR: {
                this.data = this.data.setIn(['noteData', action.key], action.value);
                const itemToUpdate = this.getNotes().findIndex(listItem => {
                    return listItem.get('id') === action.note.get('id');
                });
                const note = this.data.getIn(['notes', itemToUpdate]);
                const updateNote = note.set('color', action.color); 
                this.data = this.data.setIn(['notes', itemToUpdate], updateNote);
                this.emit("storeUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.CREATE_NOTE: {
                this.data = this.data.update('notes', notes => notes.push(action.data));
                this.data = this.data.set('saveStatus', true);
                this.emit("storeUpdated");
                this.emit("storeNoteDataUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.UPDATE_NOTE: {
                const notes = this.getNotes();
                const itemToUpdate = notes.findIndex(listItem => {
                    return listItem.get('id') === action.data.get('id');
                });
                this.data = this.data.setIn(['notes', itemToUpdate], action.data);
                this.data = this.data.set('saveStatus', true);
                this.emit("storeUpdated");
                this.emit("storeNoteDataUpdated");
                break;
            }
            case NotesAppActions.NOTES_APP_ACTIONS.CLEAR_TRASH: {
                const ids = action.data ? action.data.map(item => item.id) : [];
                this.removeNotes(ids);
                this.emit("storeUpdated");
                break;
            }
            default: {
            }
        }
    }

    getNotes() {
        return this.data.get('notes');
    }

    showEditDialog() {
        return this.data.get('showEditDialog');
    }

    getNoteData() {
        return this.data.get('noteData');
    }

    getSaveStatus() {
        return this.data.get('saveStatus');
    }
}

const notesAppStore = new NotesAppStore();
dispatcher.register(notesAppStore.handleActions.bind(notesAppStore));
export default notesAppStore;
import dispatcher from "../dispatcher/Dispatcher";
import Immutable from "immutable";

export const NOTES_APP_ACTIONS = {
    CREATE_NOTE: 'notesAppActions.CreateNote',
    UPDATE_NOTE: 'notesAppActions.UpdateNote',
    SHOW_EDIT_DIALOG: 'notesAppActions.ShowEditDialog',
    CLOSE_EDIT_DIALOG: 'notesAppActions.CloseEditDialog',
    REMOVE_NOTE: 'notesAppActions.RemoveNote',
    DELETE_NOTE: 'notesAppActions.DeleteNote',
    LOAD_NOTES: 'notesAppActions.LoadNotes',
    SEND_BACK_NOTE: 'notesAppActions.SendBackNote',
    SET_NOTE_VALUE: 'notesAppActions.SetNoteValue',
    CLEAR_TRASH: 'notesAppActions.ClearTrash',
    SET_NOTE_COLOR: 'notesAppActions.SetNoteColor'
};

const server_url = "http://localhost:8080";

function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

function readResponseAsJSON(response) {
    return response.json();
}

function updateNote(id, payload, method, saveCallback = null) {
    fetch(server_url + "/note/" + id, {
        method: method,
        body: JSON.stringify(payload)
    })
        .then(validateResponse)
        .then(readResponseAsJSON)
        .then((response) => {
            if (saveCallback) saveCallback(response);
        }).catch(function (err) {
            console.log(err)
        })
}

function clearNotes(ids, saveCallback = null) {
    fetch(server_url + "/clear", {
        method: 'DELETE',
        body: JSON.stringify(ids)
    })
        .then(validateResponse)
        .then(readResponseAsJSON)
        .then((response) => {
            if (saveCallback) saveCallback(response);
        }).catch(function (err) {
            console.log(err)
        })
}

export function create(note) {
    const payload = {
        type: note.get('type'),
        status: 'active',
        title: note.get('title'),
        content: note.get('content')
    };
    fetch(server_url + "/note", {
        method: 'POST',
        body: JSON.stringify(payload)
    }).then(validateResponse)
        .then(readResponseAsJSON)
        .then((response) => {
            dispatcher.dispatch({
                type: NOTES_APP_ACTIONS.CREATE_NOTE,
                data: Immutable.fromJS(response)
            })
        }).catch(function (err) {
            console.log(err)
        })
}

export function update(data) {
    const payload = {
        type: data.get('type'),
        status: 'active',
        title: data.get('title'),
        content: data.get('content')
    };
    function postUpdate(data) {
        dispatcher.dispatch({
            type: NOTES_APP_ACTIONS.UPDATE_NOTE,
            data: Immutable.fromJS(data)
        })
    }
    updateNote(data.get('id'), payload, 'PUT', postUpdate);
}

export function setValue(key, value) {
    dispatcher.dispatch({
        type: NOTES_APP_ACTIONS.SET_NOTE_VALUE,
        key,
        value
    })
}

export function setColor(note, color) {
    dispatcher.dispatch({
        type: NOTES_APP_ACTIONS.SET_NOTE_COLOR,
        note,
        color
    })
}

export function load() {
    fetch(server_url + "/note")
        .then(readResponseAsJSON)
        .then(responseData => {
            dispatcher.dispatch({
                type: NOTES_APP_ACTIONS.LOAD_NOTES,
                data: Immutable.fromJS(responseData)
            })
        });
}

export function openEditDialog(data) {
    dispatcher.dispatch({
        type: NOTES_APP_ACTIONS.SHOW_EDIT_DIALOG,
        data
    })
}

export function clearTrash(deletedNoteIds) {
    function postUpdate(data) {
        dispatcher.dispatch({
            type: NOTES_APP_ACTIONS.CLEAR_TRASH,
            data
        })
    }
    clearNotes(deletedNoteIds, postUpdate);
}

export function closeEditDialog() {
    dispatcher.dispatch({
        type: NOTES_APP_ACTIONS.CLOSE_EDIT_DIALOG,
    })
}

export function sendBack(id) {
    const payload = {
        status: 'active'
    };
    function postUpdate(data) {
        dispatcher.dispatch({
            type: NOTES_APP_ACTIONS.SEND_BACK_NOTE,
            data: Immutable.fromJS(data)
        })
    }
    updateNote(id, payload, 'PUT', postUpdate);
}

export function remove(id) {
    const payload = {
        status: 'removed'
    };
    function postUpdate(data) {
        dispatcher.dispatch({
            type: NOTES_APP_ACTIONS.REMOVE_NOTE,
            data: Immutable.fromJS(data)
        })
    }
    updateNote(id, payload, 'DELETE', postUpdate);
}

export function deleteNote(id) {
    const payload = {
        status: 'deleted'
    };
    function postUpdate(data) {
        dispatcher.dispatch({
            type: NOTES_APP_ACTIONS.DELETE_NOTE,
            data: Immutable.fromJS(data)
        })
    }
    updateNote(id, payload, 'PUT', postUpdate);
}
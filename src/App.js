import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from './components/menu';
import NoteList from './components/note-list';
import NoteEditDialog from './components/note-edit-dialog';
import * as NotesAppActions from "./actions/NotesAppActions";
import './App.css';

class App extends Component {
  componentWillMount() {
    // initiate data load to the store
    NotesAppActions.load();
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Menu />
          <Switch>
            <Route path="/notes/:note_type" component={NoteList} />
            <Route path="/notes" component={NoteList} />
            <Route path="/" component={NoteList} />
          </Switch>
          <NoteEditDialog />
        </div>
      </Router>
    );
  }
}

export default App;

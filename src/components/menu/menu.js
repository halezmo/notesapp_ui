import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import NotesAppStore from "./../../stores/NotesAppStore";
import './menu.css';

class Menu extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            notes: NotesAppStore.getNotes()
        }
    }

    componentDidMount() {
        NotesAppStore.on("storeUpdated", this.updateNotesData);
    }

    updateNotesData = () => {
        this.setState({ notes: NotesAppStore.getNotes() })
    };

    locationMatch = (location) => {
        return this.props.location.pathname === location ? 'active' : null;
    }

    render() {
        return (
            <div className="menu">
                <div className="logo" />
                <div className="menu-items">
                    <ul>
                        <li>
                            <Link
                                to={`/`}
                                className={'menu-all ' + this.locationMatch('/')}
                            >
                                <span className='icon icon-all-notes' />
                                All notes
                            </Link>
                        </li>
                        <li>
                            <Link
                                to={`/notes/note`}
                                className={'menu-filter menu-filter-notes ' + this.locationMatch('/notes/note')}
                            >
                                <span className='icon icon-notes' />
                                Notes
                            </Link>
                        </li>
                        <li>
                            <Link
                                to={`/notes/image`}
                                className={'menu-filter menu-filter-images ' + this.locationMatch('/notes/image')}
                            >
                                <span className='icon icon-images' />
                                Images
                            </Link>
                        </li>
                        <li>
                            <Link
                                to={`/notes/link`}
                                className={'menu-filter menu-filter-links ' + this.locationMatch('/notes/link')}
                            >
                                <span className='icon icon-link' />
                                Links
                            </Link>
                        </li>
                        <li className='menu-separator' />
                        <li className='menu-trash'>
                            <Link
                                to={`/notes/deleted`}
                                className={this.locationMatch('/notes/deleted')}
                            >
                                <span className='icon icon-trash' />
                                Trash
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default withRouter(Menu);
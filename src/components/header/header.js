import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import * as NotesAppActions from "./../../actions/NotesAppActions";
import './header.css';

class Header extends Component {
    static propTypes = {
        noteType: PropTypes.string,
        notes: PropTypes.object
    }

    addNote() {
        NotesAppActions.openEditDialog();
    }

    clearTrash() {
        const deletedNoteIds = this.props.notes.filter(
            (item) => item.get('status') === 'deleted'
        ).map(function(item) {
            return item.get('id');
        });
        NotesAppActions.clearTrash(deletedNoteIds);
    }

    renderContent = () => {
        let items = [];
        if (this.props.noteType === 'deleted') {
            items.push(<span className='header-title' key='trash'>Trash</span>);
            items.push(<Button key='clear-trash' onClick={() => this.clearTrash()} bsStyle="danger">Clear trash</Button>);
        } else {
            items.push(<span className='header-title' key='notes'>Notes</span>);
            items.push(<Button key='add-new' onClick={() => this.addNote()} bsStyle="success">Add new note</Button>);
        }
        return items;
    }

    render() {
        return (
            <div className="header">
                {this.renderContent()}
            </div>
        );
    }
}
export default Header;
import React, { Component } from 'react';
import Note from "./../note";
import Header from './../header';
import NotesAppStore from "./../../stores/NotesAppStore";
import './note-list.css';


class NoteList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: NotesAppStore.getNotes(),
            note_type: this.props.match.params.note_type
        }
    }

    componentWillReceiveProps() {
        this.setState((prevState, props) => {
            return { note_type: props.match.params.note_type };
        });
    }

    componentDidMount() {
        NotesAppStore.on("storeUpdated", this.updateNotesData);
    }

    updateNotesData = () => {
        this.setState({ notes: NotesAppStore.getNotes() })
    };

    notes = () => {
        let noteList = this.state.notes;
        if (this.state.note_type) {
            if (this.state.note_type === 'deleted') {
                noteList = noteList.filter(
                    (item) => item.get('status') === this.state.note_type
                );

            } else {
                noteList = noteList.filter(
                    (item) => item.get('type') === this.state.note_type && item.get('status') === 'active'
                );
            }
        } else {
            noteList = noteList.filter(
                (item) => item.get('status') === 'active'
            );
        }

        let notesToRender = [];
        noteList.forEach((note) => {
            notesToRender.push(<Note key={note.get('id')} note={note} />);
        });
        return notesToRender.length > 0 ? notesToRender : (<div className="no-notes"/>);
    }

    render() {
        return (
            <div className='content'>
                <Header
                    notes={this.state.notes} 
                    noteType={this.state.note_type} 
                />
                <div className="note-list">
                    {this.notes()}
                </div>
            </div>
        );
    }
}
export default NoteList;
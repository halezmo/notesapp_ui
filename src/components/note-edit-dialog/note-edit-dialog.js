import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import * as NotesAppActions from "./../../actions/NotesAppActions";
import NotesAppStore from "./../../stores/NotesAppStore";
import './note-edit-dialog.css';

class NoteEditDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: NotesAppStore.showEditDialog(),
            note: NotesAppStore.getNoteData(),
            isSaved: NotesAppStore.getSaveStatus(),
            content: '',
            title: ''
        }
    }

    componentDidMount() {
        NotesAppStore.on("storeDialogUpdated", this.updateDialogData);
        NotesAppStore.on("storeNoteDataUpdated", this.updateNoteData);
    }

    updateNoteData = () => {
        const note = NotesAppStore.getNoteData();
        this.setState({
            note: note,
            isSaved: NotesAppStore.getSaveStatus(),
            content: note.get('content') || '',
            title: note.get('title') || ''
        })
    }

    updateDialogData = () => {
        this.setState({
            isOpen: NotesAppStore.showEditDialog()
        })
    }

    closeDialog() {
        NotesAppActions.closeEditDialog();
    }

    isEdit() {
        return Boolean(this.getNoteValue('id'));
    }

    save() {
        if (this.isEdit()) {
            NotesAppActions.update(this.state.note);
        } else {
            NotesAppActions.create(this.state.note);
        }
    }

    onValueChange(event) {
        NotesAppActions.setValue(event.currentTarget.name, event.currentTarget.value);
    }

    renderTitle = () => {
        if (this.isChecked('note')) {
            return (
                <div>
                    <input
                        className="note-title-input"
                        type="text"
                        placeholder="Title"
                        onChange={this.onValueChange}
                        value={this.state.title}
                        name="title" />
                </div>
            );
        }
        return null;
    }

    getNoteValue = (key) => {
        if (this.state.note) {
            return this.state.note.get(key);
        }
        return null;
    }

    isChecked = (value) => {
        return this.getNoteValue('type') === value;
    }

    renderTypes = () => {
        return (
            <div className='note-type-selection'>
                <div>{this.isEdit() ? null : 'Choose what kind of note you would like to add:'}</div>
                <span>
                    <input type="radio" checked={this.isChecked("note")} onChange={this.onValueChange} name="type" value="note" />
                    <label>Note</label>
                </span>
                <span>
                    <input type="radio" checked={this.isChecked("image")} onChange={this.onValueChange} name="type" value="image" />
                    <label>Image</label>
                </span>
                <span>
                    <input type="radio" checked={this.isChecked("link")} onChange={this.onValueChange} name="type" value="link" />
                    <label>Links</label>
                </span>
            </div>
        );
    }

    contentPlaceholder = () => {
        const type = this.getNoteValue('type');
        let label = ''
        if (type === 'note') {
            label = 'Take a note...';
        }
        else if (type === 'image') {
            label = 'Enter image URL';
        }
        else if (type === 'link') {
            label = 'Enter URL';
        }
        return label;
    }

    renderModalContent = () => {
        if (this.state.isSaved) {
            const message = this.getNoteValue('id') ? 'updated' : 'added';
            return (
                <Modal.Body>
                    Note has been {message}
                </Modal.Body>
            );
        }
        else {
            return (
                <Modal.Body>
                    {this.renderTypes()}
                    <div>
                        {this.renderTitle()}
                        <div>
                            <textarea
                                className='note-content-input'
                                placeholder={this.contentPlaceholder()}
                                onChange={this.onValueChange}
                                name="content"
                                value={this.state.content} />
                        </div>
                    </div>
                </Modal.Body>
            );
        }
    }

    renderActions = () => {
        if (this.state.isSaved) {
            return (
                <Modal.Footer>
                    <Button onClick={() => this.closeDialog()}>Close</Button>
                </Modal.Footer>
            );
        } else {
            const saveLabel = this.getNoteValue('id') ? 'Save' : 'Add a note';
            return (
                <Modal.Footer>
                    <Button onClick={() => this.save()} bsStyle="primary">{saveLabel}</Button>
                    <Button onClick={() => this.closeDialog()}>Cancel</Button>
                </Modal.Footer>
            );
        }
    }

    render() {
        const title = this.getNoteValue('id') ? 'Edit note' : 'Add note';
        return (
            <Modal show={this.state.isOpen} animation={false}>
                <Modal.Header>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                {this.renderModalContent()}
                {this.renderActions()}
            </Modal>
        );
    }
}
export default NoteEditDialog;
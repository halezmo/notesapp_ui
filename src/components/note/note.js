import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as NotesAppActions from "./../../actions/NotesAppActions";
import './note.css';

const defaultBackgroundColor = '#ffffff';

class Note extends Component {
    static propTypes = {
        note: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            showColorPicker: false
        }
    }

    noteId() {
        return this.props.note.get('id');
    }

    sendBack() {
        NotesAppActions.sendBack(this.noteId());
    }

    remove() {
        NotesAppActions.remove(this.noteId());
    }

    delete() {
        NotesAppActions.deleteNote(this.noteId());
    }

    edit() {
        NotesAppActions.openEditDialog(this.props.note);
    }

    colorPick() {
        this.setState({ showColorPicker: true })
    }

    colorPickEnabled() {
        return this.noteType() === 'note';
    }

    noteBoxStyle = () => {
        if (this.noteType() === 'image') {
            return {
                backgroundColor: defaultBackgroundColor,
                backgroundImage: `url("${this.noteContent()}")`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center center',
                height: '200px'
            };
        }
        if (this.noteType() === 'note' || this.noteType() === 'link') {
            return {
                background: this.noteColor()
            }
        }
        return null;
    }

    noteTitle() {
        return this.props.note.get('title');
    }

    noteContent() {
        return this.props.note.get('content');
    }

    noteType() {
        return this.props.note.get('type');
    }

    noteStatus() {
        return this.props.note.get('status');
    }

    noteColor() {
        return this.props.note.get('color') || defaultBackgroundColor;
    }



    colorChange(event) {
        NotesAppActions.setColor(this.props.note, event.target.value);
        this.setState({ showColorPicker: false })
    }

    get colorPicker() {
        if (this.state.showColorPicker) {
            const colorKey = "color_" + this.noteId();
            const colors = ['#ffffff', '#ffbabf', '#d7d7ff', '#e8e8e8'];
            const colorOptions = [];
            colors.forEach(color => {
                colorOptions.push(
                    <label key={`color-option-${color}`} className="container">
                        <input className='color-pick' type="radio" onChange={this.colorChange.bind(this)} name={colorKey} value={color} />
                        <span className="checkmark" style={{ background: color }} />
                    </label>
                );
            })
            return (
                <div className='note-color-picker'>
                    {colorOptions}
                </div>
            )
        }
        return null;
    }

    actionButton = (action, icon, onClick) => {
        return (
            <button className={`action-${action}`} key={action} onClick={onClick}>
                <span className={`icon icon-${icon}`} />
            </button>
        );
    }

    actionLink = (action, label, onClick) => {
        return (
            <a className={`action-${action}`} key={action} onClick={onClick}>
                {label}
            </a>
        );
    }

    renderActions = () => {
        let actions = [];
        if (this.noteStatus() === 'active') {
            if (this.colorPickEnabled()) {
                actions.push(this.actionButton('color', 'color', this.colorPick.bind(this)));
            }
            actions.push(this.actionButton('edit', 'edit', this.edit.bind(this)));
            actions.push(this.actionButton('delete', 'trash', this.delete.bind(this)));
        }
        if (this.noteStatus() === 'deleted') {
            actions.push(this.actionLink('send-back', 'Send back', this.sendBack.bind(this)));
            actions.push(<span key='action-separator' className='action-separator'>|</span>);
            actions.push(this.actionLink('remove', 'Remove', this.remove.bind(this)));
        }
        return actions;
    }

    renderContent = () => {
        let fields = [];
        if (this.noteType() === 'note') {
            fields.push(<p className='note-title' key='title'>{this.noteTitle()}</p>)
            fields.push(<p className='note-content' key='content'>{this.noteContent()}</p>)
        }
        if (this.noteType() === 'link') {
            fields.push(
                <span key='link-content-text'>{this.noteContent()}</span>)
            fields.push(
                <a key='link-content' className='link-content' href={this.noteContent()} target='_blank'>
                    <span className='icon icon-new-tab' />
                </a>)
        }
        return fields;
    }

    render() {
        return (
            <div key={this.noteId()} className="note" style={this.noteBoxStyle()}>
                <div className="note-data">
                    {this.renderContent()}
                </div>
                <div className='note-action'>
                    {this.renderActions()}
                </div>
                {this.colorPicker}
            </div>
        );
    }
}
export default Note;